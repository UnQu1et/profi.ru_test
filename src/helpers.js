export const classNames = (...args) =>
  args
    .reduce((acc, cur) => {
      const className = cur && cur.trim();
      if (className) {
        acc.push(className);
      }
      return acc;
    }, [])
    .join(' ');
