import React, { useState } from 'react';
import stations from './data.json';
import './normalize.css';
import './App.css';
import Nav from './Nav';
import Stations from './Stations';

const navs = [
  {
    name: 'Списком',
    value: 'list'
  },
  {
    name: 'Плиткой',
    value: 'tile'
  }
];

function App() {
  const [look, setLook] = useState('list');

  return (
    <div className="app">
      <header className="app__header header">
        <Nav className="header__nav" current={look} onChange={setLook} navs={navs} />
      </header>
      <main className="app__content">
        <Stations className="app__stations" look={look} stations={stations} />
      </main>
    </div>
  );
}

export default App;
