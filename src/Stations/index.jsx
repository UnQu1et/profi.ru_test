import React from 'react';
import { classNames } from 'helpers';
import './index.css';
import StationItem from './StationItem';

function Stations({ className, stations, look }) {
  return (
    <div className={classNames(className, 'stations', look === 'tile' && 'stations_look-tile')}>
      {stations.map(station => (
        <StationItem className="stations__item" look={look} key={station.name} {...station} />
      ))}
    </div>
  );
}

export default Stations;
