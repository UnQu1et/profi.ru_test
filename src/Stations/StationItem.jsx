import React from 'react';
import { classNames } from '../helpers';
import './StationItem.css';

function StationItem({ className, color, look, img, name, line, text }) {
  const isLookTile = look === 'tile';
  return (
    <div className={classNames(className, 'station-item', isLookTile && 'station-item_tile')}>
      <div className="station-item__photo-container">
        <div
          className="station-item__photo"
          style={{ backgroundImage: `url(${require(`assets/images/${img}.jpg`)})` }}
        />
      </div>
      <div className="station-item__info">
        <h2 className="station-item__name">{name}</h2>
        <span className="station-item__line" style={{ color }}>{line}</span>
        <div className="station-item__description">{text}</div>
      </div>
    </div>
  );
}

export default StationItem;
