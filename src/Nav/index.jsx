import React from 'react';
import { classNames } from 'helpers';
import './index.css';
import NavItem from './NavItem';

function Nav({ className, current, navs, onChange }) {
  return (
    <ul className={classNames(className, 'nav')}>
      {navs.map(nav => (
        <NavItem
          className="nav__item"
          active={current === nav.value}
          name={nav.name}
          value={nav.value}
          key={nav.value}
          onClick={onChange}
        />
      ))}
    </ul>
  );
}

export default Nav;
