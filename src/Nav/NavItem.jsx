import React, { useCallback } from 'react';
import { classNames } from 'helpers';
import './NavItem.css';

function NavItem({ className, active, name, value, onClick }) {
  const handleClick = useCallback(() => onClick(value), [value, onClick]);
  return (
    <li className={classNames(className, 'nav-item', active && 'nav-item_active')} onClick={handleClick}>
      <span className="nav-item__name">{name}</span>
    </li>
  );
}

export default React.memo(NavItem);
